# Geração de Assinatura CMS - Frontend (Angular Application) com uso Proxy COARS 

Aplicação CMS de exemplo que realiza a integração com o plugin BRy Extension para realizar as seguintes operações:

- Listar os certificados digitais instalados no sistema operacional;
- Realizar cifragem dos atributos assinados obtidos na etapa de inicialização e codificá-los em base64.
- Integração com API REST Assinatura Digital utilizando Proxy COARS (sem necessidade de ter um back-end).

Foi utilizado o arquivo script-customizavel.js, disponibilizado na documentação do plugin, para realizar as chamadas das funções do BRy Extension. Esse script está localizado na pasta assets do projeto. 

<b>Realiza comunicação direta com API Assinatura Digital (Framework) para assinatura CMS</b>

### Tech

O exemplo utiliza as bibliotecas JavaScript abaixo:
* [Angular] - Angular is an app-design framework and development platform for creating efficient and sophisticated single-page apps.
* [AdminLTE] - AdminLTE is a fully responsive administration template.
* [Font-Awesome] - Font Awesome is a set of font and icon tools based on CSS and LESS.
* [jquery] - jQuery is a JavaScript library designed to simplify HTML DOM tree traversal and manipulation, as well as event handling, CSS animation, and Ajax.

### Variáveis que devem ser configuradas

O exemplo por consumir a API de assinatura necessita ser configurado com token de acesso válido.

Esse token de acesso pode ser obtido através da documentação disponibilizada no [Docs da API de Assinatura](https://api-assinatura.bry.com.br) ou através da conta de usuário no [BRy Cloud](https://cloud.bry.com.br/home/usuarios/autenticado/aplicacoes).

Caso ainda não esteja cadastrado, [cadastre-se](https://www.bry.com.br/) para ter acesso a nossa plataforma de serviços.

**Observação**

Por se tratar de uma informação sensível do usuário, reforçamos que a informação inserida no exemplo é utilizada pontualmente para geração de assinatura.


## Adquirir um certificado digital

É muito comum no início da integração não se conhecer os elementos mínimos necessários para consumo dos serviços.

Para assinar digitalmente um documento, é necessário, antes de tudo, possuir um certificado digital, que é a identidade eletrônica de uma pessoa ou empresa.

O certificado, na prática, consiste em um arquivo contendo os dados referentes à pessoa ou empresa, protegidos por criptografia altamente complexa e com prazo de validade pré-determinado.


**Entendido isso, como faço para obter meu certificado digital?**

[Obtenha agora](https://certificado.bry.com.br/certificate-issue-selection) um Certificado Digital Corporativo de baixo custo para testes de integração.

Entenda mais sobre o [Certificado Corporativo](https://www.bry.com.br/blog/certificado-digital-corporativo/). 

### Uso

<img alt="Assinatura CMS" title="Interface de assinatura CMS" src="imagem/interface-assinatura.cms.png" width="400px" />

Para executar a aplicação é necessário executar os seguintes passos:

1 - Na pasta raiz do projeto, execute o comando para instalar as dependências:

    npm install

2 - Iniciar a aplicação:

    npm run start


É preciso executar o comando npm rum start, ao invés, de ng serve devido a utilização do proxy. Esse proxy é utilizado para conseguir realizar a comunicação com
o serviço rest do Framework. A configuração do proxy está definida no arquivo proxy.config.js armazenado na pasta raiz do projeto.

   [Angular]: <https://github.com/angular>
   [AdminLTE]: <https://github.com/ColorlibHQ/AdminLTE>
   [Font-Awesome]: <https://github.com/FortAwesome/Font-Awesome>
   [jquery]: <https://github.com/jquery/jquery>
