const PROXY_CONFIG = [{
    context: [
        '/api'
    ],
    target: 'https://fw2.bry.com.br/',
    secure: true,
    logLevel: 'debug',
    changeOrigin: true,
}]

module.exports = PROXY_CONFIG;