import { Component, OnInit, Input } from '@angular/core';
import { Router } from "@angular/router";
import {FormGroup, FormBuilder, FormControl, Validators, AbstractControl} from '@angular/forms'
import {SignatureService} from './signature.service'
import {tap} from 'rxjs/operators'
declare var BryApiModule: any;


@Component({
  selector: 'cms-assinatura',
  templateUrl: './signature.component.html',
  styleUrls: ['./signature.component.css']
})
export class InicializarComponent implements OnInit {


  initializationForm: FormGroup;

  inicializationResult;

  finalizationResult;

  file: any;

  signedAttributes;

  certificateBase64;

  title =  'CMS Signature';

  auxSignatureValue;

  signature;

  token

  constructor(private signatureService: SignatureService,
              private router: Router,
              private formBuilder: FormBuilder) {

  }

  ngOnInit() {

    this.initializationForm = this.formBuilder.group({
      token:  new FormControl('', { validators: [Validators.required], updateOn: 'blur'}),
      nonce:  new FormControl('1', { validators: [Validators.required], updateOn: 'blur'}),
      attached: new FormControl('true', { validators: [Validators.required], updateOn: 'blur'}),
      profile: new FormControl('BASIC', { validators: [Validators.required], updateOn: 'blur'}),
      hashAlgorithm: new FormControl('SHA256', { validators: [Validators.required], updateOn: 'blur'}),
      certificate: new FormControl(''),
      operationType: new FormControl('SIGNATURE', { validators: [Validators.required], updateOn: 'blur'}),
      documentNonce: new FormControl('1', { validators: [Validators.required], updateOn: 'blur'}),
      file: this.formBuilder.control(null, [Validators.required]),
      signature: new FormControl('')
    })

  }

  initialize(inicialization: FormGroup, certificateBase64) {

    this.token = this.initializationForm.get('token').value;

    this.certificateBase64 = certificateBase64;

    this.signatureService.initialize(inicialization, this.certificateBase64, this.file, this.token)
      .pipe(
        tap((result: any) => {
          this.inicializationResult = result;
        }
        )
      ).subscribe( async (result: any) => {

        this.signedAttributes = result.signedAttributes[0].content ;

        console.log(`Inicialization result: ${this.signedAttributes}`);

        const preparedExtensionData = await this.prepareExtensionInputData(result);

        console.log('Dados preparados da extensao:', preparedExtensionData);

        const signedData = await this.signDataByExtension(preparedExtensionData);

        console.log('Dados assinados pela extensao: ', signedData);

        var objPreparedExtensionData = JSON.parse(signedData);

        this.auxSignatureValue = objPreparedExtensionData.assinaturas[0].hashes[0];

        console.log('Encrypted signed attributes and Base64 encoded: ', this.auxSignatureValue);

        this.signatureService.finalizar(inicialization, this.certificateBase64, this.file, this.signedAttributes, this.auxSignatureValue, this.token)
            .pipe(
                 tap((result: any) => {
                  this.finalizationResult = result;
                  }
            )
        ).subscribe((result: any) => {

          console.log(`Finalization result: ${result.signatures[0].content}`);

          this.signature = JSON.stringify(result)

          this.initializationForm.get('signature').setValue(this.signature);

          alert('Signature realized successful!');

        })


      })

  }

  clear(){
    this.signature = null;
    this.file = null;
    this.initializationForm.get('file').reset()
    this.initializationForm.get('file').setValue(null);
    this.initializationForm.get('nonce').setValue('1');
    this.initializationForm.get('hashAlgorithm').setValue('SHA256');
    this.initializationForm.get('documentNonce').setValue('1');
  }


  fillCertificateDataForm() {
    BryApiModule.fillCertificateDataForm();
  }

  listCertificates() {
    BryApiModule.listCertificates();
  }

  prepareExtensionInputData(signedAttributes): any{
    const response = BryApiModule.prepararDadosEntradaExtensao(signedAttributes);
    return response;
  }

  async signDataByExtension(prepareInputData) {
    const signedData = BryApiModule.sign(prepareInputData);
    return signedData;
  }


 upload(event){
    if (event.target.files.length > 0) {
      this.file = event.target.files[0];

    }
  }

}
