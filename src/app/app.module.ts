import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { RouterModule, PreloadAllModules } from '@angular/router';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';

import { SignatureService } from './signature/signature.service'

import {ROUTES} from './app.routes';
import { AboutComponent } from './about/about.component';
import { InicializarComponent } from './signature/signature.component';
import { InputContainerComponent } from './shared/input/input-container/input-container.component'

import {ApplicationErrorHandler} from './app.error-handler'

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    AboutComponent,
    InicializarComponent,
    InputContainerComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(ROUTES, {preloadingStrategy: PreloadAllModules})
  ],
  providers: [SignatureService,
              {provide: ErrorHandler, useClass: ApplicationErrorHandler}],
  bootstrap: [AppComponent]
})
export class AppModule { }
