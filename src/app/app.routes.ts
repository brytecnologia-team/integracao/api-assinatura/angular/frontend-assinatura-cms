import {Routes} from '@angular/router'
import { AboutComponent } from './about/about.component'
import { InicializarComponent } from './signature/signature.component'

export const ROUTES: Routes = [
    {path: '', component: InicializarComponent},
    {path: 'about', component: AboutComponent}
]
