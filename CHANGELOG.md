# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## License

- Licensed under the Apache License

Empty

## [1.0.1] - 2020-04-09

### Fixed

- Fixed value of signatureValue sent in the finalization step

[1.0.1]: https://gitlab.com/brytecnologia-team/integracao/api-assinatura/javascript/frontend-assinatura-cms-/tags/1.0.1

## [1.0.0] - 2020-03-30

### Added

- Example of CMS signature. 

[1.0.0]: https://gitlab.com/brytecnologia-team/integracao/api-assinatura/javascript/frontend-assinatura-cms-/tags/1.0.0

