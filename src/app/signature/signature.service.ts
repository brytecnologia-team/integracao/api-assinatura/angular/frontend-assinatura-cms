import {Injectable} from '@angular/core'
import {HttpClient, HttpHeaders} from '@angular/common/http'
import {Observable} from 'rxjs'
import {map} from 'rxjs/operators'
import {FormGroup} from '@angular/forms'


@Injectable()
export class SignatureService{

    URL_INITIALIZE_CMS_SERVER  = '/api/cms-signature-service/v1/signatures/initialize';
    URL_FINALIZE_CMS_SERVER  = '/api/cms-signature-service/v1/signatures/finalize';
    URL_IS_AVAILABLE_CMS_SERVER = '/api/cms-signature-service/v1/isAvailable';


    constructor(private http: HttpClient){}


    initialize(initialize: FormGroup, base64Certificate, file, token): Observable<any>{
        let headersValues = new HttpHeaders();
        headersValues = headersValues.set('Authorization', `Bearer ${token}`)
        const formData = new FormData();
        formData.append('nonce', initialize.get('nonce').value);
        formData.append('attached', initialize.get('attached').value );
        formData.append('profile', initialize.get('profile').value );
        formData.append('hashAlgorithm', initialize.get('hashAlgorithm').value );
        formData.append('certificate', base64Certificate );
        formData.append('operationType', initialize.get('operationType').value );
        formData.append('originalDocuments[0][nonce]', initialize.get('documentNonce').value );
        formData.append('originalDocuments[0][content]', file );

        return this.http.post<any>(this.URL_INITIALIZE_CMS_SERVER, formData, {
            headers: headersValues,
            reportProgress: true,
            responseType: "json"
        }).pipe(map( result => result));
    }

    finalizar(finalize: FormGroup, base64Certificate, file, signedAttributes, signatureValue, token): Observable<any>{
        let headersValues = new HttpHeaders();
        headersValues = headersValues.set('Authorization', `Bearer ${token}`);
        const formData = new FormData();
        formData.append('nonce', finalize.get('nonce').value);
        formData.append('attached', finalize.get('attached').value );
        formData.append('profile', finalize.get('profile').value );
        formData.append('hashAlgorithm', finalize.get('hashAlgorithm').value );
        formData.append('certificate', base64Certificate );
        formData.append('operationType', finalize.get('operationType').value );
        formData.append('finalizations[0][nonce]', finalize.get('documentNonce').value );
        formData.append('finalizations[0][signedAttributes]', signedAttributes );
        formData.append('finalizations[0][signatureValue]', signatureValue );
        formData.append('finalizations[0][document]', file );

        return this.http.post<any>(this.URL_FINALIZE_CMS_SERVER, formData, {
            headers: headersValues,
            reportProgress: true,
            responseType: "json"
        }).pipe(map( result => result));
    }

    isAvailable(token): Observable<any>{
        let headers = new HttpHeaders();
        headers = headers.set('Authorization', `Bearer ${token}`);
        headers = headers.set('Content-Type', 'application/json');

        return this.http.get<any>(this.URL_IS_AVAILABLE_CMS_SERVER, {headers: headers}).pipe(map( result => result));
    }

}
